import XCTest

import klaraFrameworkTests

var tests = [XCTestCaseEntry]()
tests += klaraFrameworkTests.allTests()
XCTMain(tests)
